# novel

- title: ネタキャラ転生とかあんまりだ！
- title_zh: 梗角色轉生太過頭了！
- author: 音無　奏
- illust: azuタロウ
- source: http://ncode.syosetu.com/n7608cw/
- cover: https://images-na.ssl-images-amazon.com/images/I/81GLapoChvL.jpg
- publisher: syosetu
- date: 2019-05-09T01:00:00+08:00
- status: 連載
- novel_status: 0x0100

## illusts


## publishers

- syosetu

## series

- name: ネタキャラ転生とかあんまりだ！

## preface


```
打工，削減伙食費，犧牲青春反復課金。
僅把那種游戲作為武器對抗孤獨和無聊，那就是相川徹的人生。
那樣的徹在打工歸途從暴走的巴士下救了一位少女，簡單地死了。
接著醒來時，不知為何是在異世界，而且轉生成了游戲中的角色，娜哈特。
啊嘞，但是，這個，不是以中二病為原動力創作的梗角色嗎！
這是在異世界轉生的徹作為娜哈特經歷新生的物語。

バイトをし、食費を削り、青春を犠牲にして課金を繰り返す。そんなゲームだけを武器に孤独と退屈に抗う、それが相川徹の人生だった。そんな徹はバイト帰りに暴走するバスから一人の少女を助け、あっさりと死んだ。次に目を覚ました時には、何故か異世界で、それもゲームの中のキャラクター、ナハトに転生していた。あれ、でも、これ、中二病を原動力に作ったネタキャラじゃねーか！これは異世界に転生した徹がナハトとして新たな生を歩む物語。
※マッグガーデンノベルズ様より、書籍化させていただきました。第一巻が発売中です。
```

## tags

- node-novel
- R15
- syosetu
- ガールズラブ
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ファンタジー
- 主人公最強
- 書籍化
- 残酷な描写あり
- 無双
- 異世界転生
- 百合
- ＴＳ転生
- ＴＳ
-

# contribute

- 土之女神

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n7608cw

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n7608cw&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n7608cw/)
- https://masiro.moe/forum.php?mod=forumdisplay&fid=261&page=1
- [梗角色转生吧](https://tieba.baidu.com/f?kw=%E6%A2%97%E8%A7%92%E8%89%B2%E8%BD%AC%E7%94%9F&ie=utf-8 "")
- 

